package io.finer.ads.jeecg.service.impl;

import io.finer.ads.jeecg.entity.SizeType;
import io.finer.ads.jeecg.mapper.SizeTypeMapper;
import io.finer.ads.jeecg.service.ISizeTypeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 广告常规尺寸
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Service
public class SizeTypeServiceImpl extends ServiceImpl<SizeTypeMapper, SizeType> implements ISizeTypeService {

}
