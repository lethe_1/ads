package io.finer.ads.jeecg.service.impl;

import io.finer.ads.jeecg.entity.Ad;
import io.finer.ads.jeecg.mapper.AdMapper;
import io.finer.ads.jeecg.service.IAdService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 广告
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Service
public class AdServiceImpl extends ServiceImpl<AdMapper, Ad> implements IAdService {

}
