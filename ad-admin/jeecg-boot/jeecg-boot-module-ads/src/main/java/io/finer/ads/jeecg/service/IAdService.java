package io.finer.ads.jeecg.service;

import io.finer.ads.jeecg.entity.Ad;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 广告
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
public interface IAdService extends IService<Ad> {

}
